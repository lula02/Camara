package franciscolula.facci.com.camara4b;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    Button botonGuardar, botonCamara;
    ImageView imageViewFoto;
    Bitmap imageBitmap;

    private final int REQUEST_IMAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonCamara = (Button) findViewById(R.id.buttonCamara);
        botonGuardar = (Button) findViewById(R.id.buttonGuardar);
        imageViewFoto = (ImageView) findViewById(R.id.imageViewFoto);

        botonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarImagen();
            }
        });

        botonCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tomarFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (tomarFoto.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(tomarFoto, REQUEST_IMAGE);

                }
            }
        });

    }

    private final int PERMISSION_SAVE = 101;

    private void GuardarImagen() {
        //Preguntar si hay permisos de escritura en la SDCard
        if (tienePermisosSDCard()) {
            //Crear la caroeta donde voy a guardar la imagen
            //Guardar la imagen
            CrearCarpeta();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_SAVE);
        }
    }


    public boolean tienePermisosSDCard() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void CrearCarpeta() {
        String nombreCarpeta =
                Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "Imagenes4B";
        File carpeta = new File(nombreCarpeta);
        if (!carpeta.exists()) {
            Toast.makeText(this, "Carpeta ya existente", Toast.LENGTH_LONG).show();
        } else {
            if (carpeta.mkdir()) {
                Toast.makeText(this, "Carpeta Creada", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, " Carpeta no Creada", Toast.LENGTH_LONG).show();
            }
        }

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == REQUEST_IMAGE && resultCode==RESULT_OK)
            {
                Bundle extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                imageViewFoto.setImageBitmap(imageBitmap);
            }
    }
}
